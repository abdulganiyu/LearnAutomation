package com.takeScreenShot;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.library.Utility;

public class ScreenShotFacebook {
	
	
	WebDriver driver;
	@Test
	public void takeScreenShot(){
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Sekinat\\Documents\\Selenium_Jars\\chromedriver.exe");
		driver = new ChromeDriver();
						
		driver .manage().window().maximize();
		
		
		driver.get("https://www.facebook.com/");
		Utility.captureScreenShot(driver, "Browser");
		
		driver.findElement(By.xpath("//*[@id='email']")).sendKeys("Abdul-Ganiyu");
		Utility.captureScreenShot(driver, "Type Name");
		
//		TakesScreenshot ts = (TakesScreenshot)driver;
//		
//		File source = (File) ts.getScreenshotAs(OutputType.FILE);
//		
//		FileUtils.copyFile(source, new File("./Screenshot/screenshotName.png"));
//		
//		System.out.println("screenshot taken");
		
		driver.quit();
	}
	
}
