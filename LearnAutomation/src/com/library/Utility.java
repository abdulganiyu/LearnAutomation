package com.library;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Utility {
	
	public static void captureScreenShot(WebDriver driver, String screenshotName){
		
		
		try {
			TakesScreenshot ts = (TakesScreenshot)driver;
			
			File source = (File) ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(source, new File("./Screenshot/"+screenshotName+".png"));
			
			System.out.println("screenshot taken");
		
		
		} 
		catch (Exception e) 
		{
			System.out.println("Exception while taken screenshot "+ e.getMessage());			
		} 
		
		
	}

}
